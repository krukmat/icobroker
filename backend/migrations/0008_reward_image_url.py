# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0007_company_image_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='reward',
            name='image_url',
            field=models.URLField(blank=True),
        ),
    ]
