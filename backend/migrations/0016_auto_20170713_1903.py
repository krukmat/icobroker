# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0015_auto_20170713_1851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reward',
            name='period_from',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='reward',
            name='period_to',
            field=models.DateTimeField(null=True),
        ),
    ]
