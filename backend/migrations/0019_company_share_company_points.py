# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0018_auto_20170812_1608'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='share_company_points',
            field=models.IntegerField(default=0),
        ),
    ]
