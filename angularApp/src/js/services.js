angular.module('angularApp.services', ['angularApp.config', 'ngResource', 'ui.bootstrap'])
.factory('AuthService', function($window, $http, backend){
    return {
      login: function(user){
          return $http.post(backend + '/auth/login/', user);
      },
      register: function(user) {
          return $http.post(backend + '/auth/register/', user)
      },
      me: function() {
          return $http({url: backend + '/auth/me/', method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      social_account: function(social) {
          return $http({url: backend + '/social_accounts/add',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: social})
      },
      update_me: function(profile){
          return $http({url: backend + '/auth/me/',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: profile})
      },
      update_password: function(passwords){
          return $http({url: backend + '/companies/change_password',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: passwords})
      },
      rewards_to_exchange: function() {
          return $http({url: backend + '/rewardsxuser', method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      update_reward_exchange: function(item){
          return $http({url: backend + '/rewardsxuser/'+item.pk+'/commit',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      subscribers_list: function(){
          return $http({url: backend + '/subscription',  method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      update_subscription: function(subscription){
          return $http({url: backend + '/subscription/'+subscription.pk+'/update_points',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: subscription})
      },
      add_reward: function(reward){
          return $http({url: backend + '/rewards/add',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: reward})
      },
      update_reward: function(reward){
          return $http({url: backend + '/rewards/'+reward.pk+'/update',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: reward})
      }
    }
})
.factory('WhatToMineAPI', function($window, $http, backend) {
    return {
        getStats: function() {
            return $http({url: backend + '/what_to_mine/', method: 'GET'});
        },
    }
})
.factory('CryptoCompareAPI', function($window, $http, backend, $rootScope, $window){
  var socket = io.connect('https://streamer.cryptocompare.com/');


  function average(data){
      var sum = data.reduce(function(sum, value){
        return sum + value;
      }, 0);

      var avg = sum / data.length;
      return avg;
  }

  function cleanupPrices(prices){
    values = [];
    for (var market in prices ){
       values.push(prices[market])
    };
    return values;
  }
  function standardDeviation(values){

      var avg = average(values);

      var squareDiffs = values.map(function(value){
        var diff = value - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
      });

      var avgSquareDiff = average(squareDiffs);

      var stdDev = Math.sqrt(avgSquareDiff);
      return stdDev;
    }

  return {
    getEquipmentData: function() {
        return $http({url: backend + '/mining_equipment/', method: 'GET'});
    },
    getTopCurrencies: function() {
        return $http({url: backend + '/top_coins/', method: 'GET'});
    },
    subscribeToCoinMarkets: function(coin, scope){
        //Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
        //Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
        //For aggregate quote updates use CCCAGG as market
        //You can subscribe to all exchanges for a currency pair by using the following API
        var subscription;
        var currentCoin = coin;

        $.getJSON( "https://min-api.cryptocompare.com/data/subs?fsym="+currentCoin+"&tsyms=BTC", function( data ) {
         if (data['BTC'] != null){
            subscription = data['BTC']['CURRENT'];
            if (subscription.length > 1){
                socket.emit('SubAdd', {subs:subscription} );
            }
            else
                $rootScope.$broadcast('remove-coin', { coin: currentCoin });
         } else {
            $rootScope.$broadcast('remove-coin', { coin: currentCoin });
         }
        });
        this.evaluatePrice = function(scope, symbol, market){
            // Evaluar por separado el min y max
            var values = cleanupPrices(scope.current_values[symbol]);
            if (values.length > 0){
                scope.standard_deviation[symbol] = standardDeviation(values);
                scope.average_values[symbol] = average(values);
            } else {
                console.log('Arreglo de precios vacio ::::');
            }
            //scope.max_values[symbol] = {value: 0, market: ''};
            //scope.min_values[symbol] = {value: 100, market: ''};

            for (var market in scope.current_values[symbol] ){
                    if ((scope.current_values[symbol][market] > scope.max_values[symbol].value)){
                        scope.max_values[symbol] = {value: scope.current_values[symbol][market], market: market};
                        $rootScope.$broadcast('max-value-changed', { symbol: symbol, value: scope.current_values[symbol][market], market: market });
                    }
                    if ((scope.current_values[symbol][market] < scope.min_values[symbol].value)){
                        scope.min_values[symbol] = {value: scope.current_values[symbol][market], market: market};
                        $rootScope.$broadcast('min-value-changed', { symbol: symbol, value: scope.current_values[symbol][market], market: market });
                    }
            }

            $rootScope.$broadcast('new-price', {'symbol': symbol, 'market': market});
        };
        var that = this;
        // Cotizacion actual por marketplace
        socket.on("m", function(message){
            var messageType = message.substring(0, message.indexOf("~"));
            var message
            var res = {};
            if (messageType === CCC.STATIC.TYPE.CURRENT) {
                res = CCC.CURRENT.unpack(message);
                var symbol = res.FROMSYMBOL;
                var market = res.MARKET;
                if (!isNaN(res.PRICE)){
                    if (isNaN($window.operations[symbol][market]))
                        $window.operations[symbol][market]  = 1;
                    else
                        $window.operations[symbol][market] += 1;
                    scope.current_values[res.FROMSYMBOL][res.MARKET] = res.PRICE;
                    that.evaluatePrice(scope, res.FROMSYMBOL, res.MARKET);
                }
            }

        });
    },
    subscribeToCoinMarketsTrades: function(coin){
        //Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
        //Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
        //For aggregate quote updates use CCCAGG as market
        //You can subscribe to all exchanges for a currency pair by using the following API
        var subscription;

        $.getJSON( "https://min-api.cryptocompare.com/data/subs?fsym="+coin+"&tsyms=BTC", function( data ) {
         subscription = data['BTC']['TRADE'];
         socket.emit('SubAdd', {subs:subscription} );
         console.log(data);
        });
        // Cotizacion actual por marketplace
        socket.on("m", function(message){
            var messageType = message.substring(0, message.indexOf("~"));
            var res = {};
            if (messageType === CCC.STATIC.TYPE.TRADE) {
                res = CCC.TRADE.unpack(message);
                console.log(res);
            }

        });
    }
  }
}).factory('NotificationCenter', function($http, $timeout, $window) {
  var data = { response: {}, calls: 0 };
  var poller = function() {
      for (var symbol in $window.operations){
        for (var market in $window.operations[symbol])
            $window.operations[symbol][market] = 0;
      }
      console.log('ping');
      $timeout(poller, 100000);
  };
  poller();
  return true;
});
angular.module('angularApp.services').service('modalService', ['$modal',
    function ($modal, templateUrl) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.points = 0;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close($scope);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

    }]);